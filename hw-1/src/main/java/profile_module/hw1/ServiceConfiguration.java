package profile_module.hw1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import profile_module.hw1.dao.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static profile_module.hw1.constants.DatabaseConstants.*;

@Configuration
public class ServiceConfiguration {
    @Bean
    @Scope("singleton")
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME,
                DB_USER,
                DB_PASSWORD);
    }
}
