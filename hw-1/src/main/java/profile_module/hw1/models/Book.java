package profile_module.hw1.models;

import lombok.*;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class Book {
    private Integer id;
    private String title;
    private String author;
    private LocalDate dateAdded;
}