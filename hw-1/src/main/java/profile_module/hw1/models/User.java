package profile_module.hw1.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.util.Date;
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class User {
    private Integer id;
    private String surname;
    private String name;
    private LocalDate dateOfBirth;
    private String phoneNumber;
    private String email;
    private Integer[] bookIds;
}
