package profile_module.hw1.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import profile_module.hw1.Database.BookRowMapper;
import profile_module.hw1.Database.UserRowMapper;
import profile_module.hw1.models.Book;
import profile_module.hw1.models.User;

import java.util.List;

import static profile_module.hw1.constants.DatabaseConstants.*;

@Slf4j
@Component
public class UserDao implements IDaoLayer<User, Integer> {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public User get(Integer id) {
        try {
            SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
            return jdbcTemplate.queryForObject(GET_USER_BY_ID, namedParameters, new UserRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            System.out.println("user id = " + id + " не найден");
        }
        return null;
    }

    @Override
    public List<User> getAll() {
        return jdbcTemplate.query(GET_ALL_USERS, new UserRowMapper());
    }

    @Override
    public void create(User user) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("surname", user.getSurname())
                .addValue("name", user.getName())
                .addValue("date_of_birth", user.getDateOfBirth())
                .addValue("email", user.getEmail())
                .addValue("phone_number", user.getPhoneNumber())
                .addValue("book_ids", user.getBookIds());

        jdbcTemplate.update(CREATE_USER, namedParameters);
    }

    @Override
    public void update(User user, Integer id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("surname", user.getSurname())
                .addValue("name", user.getName())
                .addValue("date_of_birth", user.getDateOfBirth())
                .addValue("email", user.getEmail())
                .addValue("phone_number", user.getPhoneNumber())
                .addValue("book_ids", user.getBookIds())
                .addValue("id", id);

        jdbcTemplate.update(UPDATE_USER, namedParameters);
    }

    @Override
    public void delete(Integer id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
        jdbcTemplate.update(DELETE_USER, namedParameters);
    }

    /**
     * Ищет пользователя по электронной почте и возвращает список его книг.
     *
     * @param email почта.
     * @return List<Book> or null.
     */
    public List<Book> getUserBooksByEmail(String email) {
        SqlParameterSource namedParameters = new MapSqlParameterSource("email", email);
        return jdbcTemplate.query(GET_USER_BOOKS_BY_EMAIL, namedParameters, new BookRowMapper());
    }
}
