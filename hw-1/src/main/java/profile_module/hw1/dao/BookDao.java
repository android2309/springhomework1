package profile_module.hw1.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import profile_module.hw1.Database.BookRowMapper;
import profile_module.hw1.models.Book;

import java.util.*;

import static profile_module.hw1.constants.DatabaseConstants.*;

@Slf4j
@Component
public class BookDao implements IDaoLayer<Book, Integer> {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Book get(Integer id) {
        try {
            SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
            return jdbcTemplate.queryForObject(GET_BOOK_BY_ID, namedParameters, new BookRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            System.out.println("book id = " + id + " не найден");
        }
        return null;
    }

    @Override
    public List<Book> getAll() {
        return jdbcTemplate.query(GET_ALL_BOOKS, new BookRowMapper());
    }

    @Override
    public void create(Book book) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("title", book.getTitle());
        paramMap.put("author", book.getAuthor());
        paramMap.put("date_added", book.getDateAdded());

        jdbcTemplate.update(CREATE_BOOK, paramMap);
    }

    @Override
    public void update(Book book, Integer id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("title", book.getTitle())
                .addValue("author", book.getAuthor())
                .addValue("date_added", book.getDateAdded())
                .addValue("id", id);

        jdbcTemplate.update(UPDATE_BOOK, namedParameters);
    }

    @Override
    public void delete(Integer id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
        jdbcTemplate.update(DELETE_BOOK, namedParameters);
    }
}
