package profile_module.hw1.dao;

import java.sql.SQLException;
import java.util.List;

public interface IDaoLayer<T, ID> {
    T get(ID id) throws SQLException;

    List<T> getAll() throws SQLException;

    void create(T newObject) throws SQLException;

    void update(T updatedObject, ID id) throws SQLException;

    void delete(ID id) throws SQLException;
}
