package profile_module.hw1;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import profile_module.hw1.dao.BookDao;
import profile_module.hw1.dao.UserDao;
import profile_module.hw1.models.Book;
import profile_module.hw1.models.User;

import java.time.LocalDate;
import java.util.List;


@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class Hw1Application implements CommandLineRunner {
    private final UserDao userDao;
    private final BookDao bookDao;
    public static void main(String[] args) {
        SpringApplication.run(Hw1Application.class, args);
    }

    @Override
    public void run(String... args) {
        seedDatabase();

        User userNew = new User(
                null, "name_1new", "surname_1new", LocalDate.now(),
                "12343321", "user1@mail.com", new Integer[]{1, 2, 3, 4, 5}
        );

        User userUpdated = new User(
                null, "name_1_updated", "surname_1_updated", LocalDate.now(),
                "12343321_updated", "user1@mail.com_updated", new Integer[]{1, 2, 3}
        );

        List<Book> books = bookDao.getAll();
        books.forEach(System.out::println);
        System.out.println(bookDao.get(7));
        var book = new Book(null, "newbook", "asda", LocalDate.now());
        bookDao.create(book);
        bookDao.update(book, 1);
        books = bookDao.getAll();
        bookDao.delete(1);
        books.forEach(System.out::println);

        userDao.update(userUpdated, 1);
        System.out.println(userDao.get(1));
        userDao.delete(1);
        userDao.create(userNew);
        var users = userDao.getAll();
        users.forEach(System.out::println);

        List<Book> userBooks = userDao.getUserBooksByEmail("user1@mail.com");
        userBooks.forEach(System.out::println);
    }

    private void seedDatabase() {
        if(!bookDao.getAll().isEmpty())
            return;

        for (int i = 0; i < 20; i++) {
            Book book = new Book(null, "book_" + i, "author_" + i, LocalDate.now());
            bookDao.create(book);
        }
    }
}
