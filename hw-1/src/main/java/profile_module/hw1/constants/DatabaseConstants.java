package profile_module.hw1.constants;

public interface DatabaseConstants {
    String DB_USER = "postgres";
    String DB_PASSWORD = "postgres";
    String DB_HOST = "localhost";
    String DB_PORT = "5432";
    String DB_NAME = "postgresLocal";
    String GET_ALL_BOOKS = "select * from books";
    String GET_BOOK_BY_ID = "select * from books where id = :id";
    String CREATE_BOOK = """
            insert into books (title, author, date_added)
            values (:title,:author,:date_added)
            """;
    String UPDATE_BOOK = """
            update books set
                title = :title, author = :author, date_added = :date_added where id = :id
            """;
    String DELETE_BOOK = "delete from books where id = :id";
    String GET_USER_BY_ID = "select * from users where id = :id";
    String GET_ALL_USERS = "select * from users";
    String CREATE_USER = """
            insert into users (surname, name, date_of_birth, phone_number,email, book_ids)
            values (:surname, :name, :date_of_birth, :phone_number, :email, :book_ids)
            """;
    String UPDATE_USER = """
            update users set
                surname=:surname, name=:name, date_of_birth=:date_of_birth, phone_number=:phone_number,
                email=:email, book_ids=:book_ids where id = :id
            """;
    String DELETE_USER = "delete from users where id = :id";
    String GET_USER_BOOKS_BY_EMAIL = """
                select id, title, author, date_added
                from (select book_ids
                      from users
                      where email = :email)
                         right join books b
                                    on b.id = any (book_ids)
                where book_ids is not null
                """;
}
