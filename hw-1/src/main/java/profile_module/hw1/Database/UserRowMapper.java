package profile_module.hw1.Database;

import org.springframework.jdbc.core.RowMapper;
import profile_module.hw1.models.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new User(
                rs.getInt("id"),
                rs.getString("surname"),
                rs.getString("name"),
                rs.getDate("date_of_birth").toLocalDate(),
                rs.getString("email"),
                rs.getString("phone_number"),
                (Integer[]) rs.getArray("book_ids").getArray()
        );
    }
}