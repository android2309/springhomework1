package profile_module.hw1.Database;

import org.springframework.jdbc.core.RowMapper;
import profile_module.hw1.models.Book;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookRowMapper implements RowMapper<Book> {
    @Override
    public Book mapRow(ResultSet rs, int rowNum) throws SQLException {

        return new Book(
                rs.getInt("id"),
                rs.getString("author"),
                rs.getString("title"),
                rs.getDate("date_added").toLocalDate()
        );
    }
}
